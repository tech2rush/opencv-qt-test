#include "imagefborenderer.h"

ImageFBORenderer::ImageFBORenderer()
{
    imageRenderer.initialize();
}

QOpenGLFramebufferObject *ImageFBORenderer::createFramebufferObject(const QSize &size)
{
    QOpenGLFramebufferObjectFormat format;
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    return new QOpenGLFramebufferObject(size, format);
}

void ImageFBORenderer::render()
{
    imageRenderer.render();
    imageRenderer.update();
}
