#include "guiapplication.h"

#include <QDebug>

int main(int argc, char** args)
{
    if( QT_VERSION < QT_VERSION_CHECK( 5, 8, 0 ) )
    {
        QString errorMessage = QString("QT version 5.8 required.");
        qCritical() << errorMessage;
        return 0;
    }
    else
        qDebug() << QString( "QT version" ) << QT_VERSION_STR;

    GuiApplication a(argc, args);

    return a.exec();
}
