#include "guiapplication.h"

#include <memory>

GuiApplication::GuiApplication(int &argc, char **args)
    : QGuiApplication(argc,args)
{
    window = new QQuickView;

    std::string filename = "/home/jaran/hin/opencv_qt_test/testapp/qml/tiger.png";

    ip = std::make_shared<ImageProvider>();//Not a Q_OBJECT
    ip->loadCVImage(filename);

    qmlRegisterType<ImageFBO>("imagefbo", 0, 1, "ImageFBO");

    window->engine()->addImageProvider(QString("provider"), ip.get());

    window->setSource(QUrl("qrc:///main.qml"));



    root = window->rootObject();
    connect( root, SIGNAL(qmlSignal(QString)),
             this, SLOT(qmlMessageParser(QString)));

    connect(this, &QGuiApplication::lastWindowClosed,
            this, &QGuiApplication::quit);

    window->setResizeMode(QQuickView::SizeRootObjectToView);
    window->show();
}

GuiApplication::~GuiApplication(){
}

void GuiApplication::qmlMessageParser(const QString &msg)
{
    qDebug() << "Received message from QML " << msg;

    if (msg == QString("greyscale"))
    {
        ip->displayGray();
        imageChanged();
    }
    if (msg == QString("colour"))
    {
        ip->displayRGB();
        imageChanged();
    }
}

void GuiApplication::imageChanged()
{
    QMetaObject::invokeMethod(root, "reload");
}

