#include "imageprovider.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

ImageProvider::ImageProvider()
    : QQuickImageProvider(QQuickImageProvider::Image)
{

}

ImageProvider::~ImageProvider()
{

}


QImage ImageProvider::requestImage(const QString &id, QSize *size, const QSize &requestedSize)
{
    if (d == displayformat::GRAYSCALE) return qImage_gray;
    else return qImage_rgb;
}

void ImageProvider::loadCVImage(std::string filename)
{
    cv::Mat cv_image_in = cv::imread(filename, CV_LOAD_IMAGE_COLOR);
    cv::cvtColor(cv_image_in, cv_image_rgb, CV_BGR2RGB);//cv_image must be kept in memory
    cv::cvtColor(cv_image_in, cv_image_gray, CV_BGR2GRAY);
    qImage_rgb = QImage((uchar*) cv_image_rgb.data, cv_image_rgb.cols, cv_image_rgb.rows, QImage::Format_RGB888);
    qImage_gray = QImage((uchar*) cv_image_gray.data, cv_image_gray.cols, cv_image_gray.rows, QImage::Format_Grayscale8);
}

void ImageProvider::displayRGB()
{
    d = displayformat::RGB;
}

void ImageProvider::displayGray()
{
    d = displayformat::GRAYSCALE;
}


