#ifndef IMAGEPROVIDER_H
#define IMAGEPROVIDER_H

#include <QQuickImageProvider>
#include <QImage>
#include <opencv2/core/core.hpp>

class ImageProvider : public QQuickImageProvider
{
public:
    enum displayformat{
        RGB,
        GRAYSCALE
    };

    ImageProvider();
    ~ImageProvider();
    QImage requestImage(const QString &id, QSize *size, const QSize &requestedSize) override;
    void loadCVImage(std::string filename);
    void displayRGB();
    void displayGray();
protected:
    QImage qImage_rgb;
    QImage qImage_gray;
    cv::Mat cv_image_rgb;
    cv::Mat cv_image_gray;
    displayformat d = displayformat::RGB;
};


#endif //IMAGEPROVIDER_H
