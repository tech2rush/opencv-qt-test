// Based on example code by Laszlo Agocs
// http://blog.qt.io/blog/2015/05/11/integrating-custom-opengl-rendering-with-qt-quick-via-qquickframebufferobject/


#ifndef IMAGEFBORENDERER_H
#define IMAGEFBORENDERER_H

#include <QQuickFramebufferObject>
#include <QOpenGLFramebufferObjectFormat>

class ImageFBORenderer: public QQuickFramebufferObject::Renderer
{
public:
    ImageFBORenderer();
    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size);
    void render();
    ImageRenderer imageRenderer;
protected:
};

#endif//IMAGEFBORENDERER_H
