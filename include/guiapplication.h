#ifndef GUIAPPLICATION_H
#define GUIAPPLICATION_H

#include <QGuiApplication>
#include <QtQuick>
#include <QQuickView>
#include <QDebug>
#include "imageprovider.h"
#include "imagefbo.h"
#include <memory>

class GuiApplication : public QGuiApplication
{
    Q_OBJECT
public slots:
    void qmlMessageParser(const QString &msg);

public:
   explicit GuiApplication(int& argc, char** args);
    ~GuiApplication();
protected:
    QQuickView *window;
    std::shared_ptr<ImageProvider> ip;
    QObject *root;

    void imageChanged();
};

#endif
