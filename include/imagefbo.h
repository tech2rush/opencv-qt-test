#ifndef IMAGEFBO_H
#define IMAGEFBO_H

#include <QQuickFramebufferObject>
#include "imagefborenderer.h"

class ImageFBO: public QQuickFramebufferObject
{
    Q_OBJECT
public:
    QQuickFramebufferObject::Renderer *createRenderer() const
    {
        return new ImageFBORenderer;
    }
protected:
};

#endif// IMAGEFBO_H
