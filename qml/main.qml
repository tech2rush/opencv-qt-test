import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Dialogs 1.2
import QtQuick.Window 2.0
import QtQuick.Controls 1.4
import imagefbo 0.1

Rectangle {
    id: root
    width: 800
    height: 600
    color: "lightgray"

    signal qmlSignal(string msg)

    ColumnLayout{
        anchors.fill: parent
        spacing: 10

        RowLayout {
            id: toolbar
            Button {
                id: nextImageButton
                text: "Greyscale"
                onClicked: {
                    if (text == "Greyscale"){
                        console.log("Greyscale button pressed");
                        root.qmlSignal("greyscale");
                        text = "Colour";
                    }
                    else{
                        console.log("Colour button pressed");
                        text = "Greyscale";
                        root.qmlSignal("colour");
                    }
                }
            }
        }

        RowLayout{
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: 10

            Image {//Note two Images using the same source are unable to reload() b/c reasons
                id: imageProviderImage
                Layout.fillHeight: true
                Layout.fillWidth: true
                fillMode: Image.PreserveAspectFit
                source: "image://provider/image"


                //Force reload
                function reload() {
                    var oldSource = source;
                    source = "";
                    source = oldSource;
                }
            }
            Image {
                Layout.fillHeight: true
                Layout.fillWidth: true
                fillMode: Image.PreserveAspectFit
                //source: "image://provider/image"
                //Note: using the same provider/image prevents reload of changed image
                //Different sources works fine eg. like below
                source: "https://forum.qt.io/logo.png"
            }
            ImageFBO {
                height: 400
                width: 200
                /*
                Rectangle{
                    anchors.fill: parent
                    color: "blue"
                }
                */
            }
       }
    }

    function reload(){
        imageProviderImage.reload();
    }

}

